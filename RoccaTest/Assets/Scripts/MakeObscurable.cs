﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeObscurable : MonoBehaviour {

	// Use this for initialization
	void Start () {
        // get all renderers in this object and its children:
        var renderers = GetComponentsInChildren<Renderer>();
        foreach (var render in renderers)
        {
            render.material.renderQueue = 2002; // set their renderQueue
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
