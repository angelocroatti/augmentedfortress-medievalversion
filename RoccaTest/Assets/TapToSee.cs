﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapToSee : MonoBehaviour {

    public Material invisible;
    public Material visible;

    public GameObject planeGenerator;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        Material changeMaterial;
        if (Input.touchCount > 0)
        {
            changeMaterial = visible;

            if (Controller.isRoccaInstantiated())
            {
                planeGenerator.SetActive(true);
            }
            
        }
        else
        {
            changeMaterial = invisible;

            if (Controller.isRoccaInstantiated())
            {
                planeGenerator.SetActive(false);
            }
        }

        foreach (var item in GameObject.FindGameObjectsWithTag("Invisible"))
        {
            item.GetComponent<Renderer>().material = changeMaterial;
        }
    }
        
}
